*********************************************************
Yuchen Yang(yy227)
1.25.2019

This is ECE651 Assignment 2. I wrote and compiled this in Eclipse so it's better if you can test it in same enviroment.

In this program, I created a Person class, which contains the basic information of a person. The BlueDevil class is the subclass of Person, containing any people related to Duke University. Student class and Instructor class are  subclasses of BlueDevil.

There are three functions inside my program, which you cna find in the mainclass.

When you test "whoIs" function, and type in your name(if in the dataset), the related information should be printed out.
e.g. 
	Input:
	whoIs("Yuchen Yang",myList);

	Output:
	Yuchen Yang is from China. She graduated from Harbin Institute of Technology and does not have work experience before.
	She enjoys dancing and skiing the most during weekend.
	Yuchen Yang is in Duke University now, her major is ECE and her netid is yy227.


When you test "countGender()" function, the total amount of male and female should be printed out.
e.g. 
	Input:
	countGender(myList);
	
	Output:
	This dataset has 3 females and 5 males.


When you test "hasWork()" function, the name of those who has work experience should be printed out.
e.g. 
	Input:
	hasWork(myList);
	
	Output:
	These people have work experience: 
	1.Adel Fahmy
	2.Ric Telford
	3.Yuanyuan Yu
	4.You Lyu
	5.Zhongyu Li
	6.Lei Chen

***********************************************************