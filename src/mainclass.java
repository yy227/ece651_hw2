import java.util.*;

class mainclass {
	public static void whoIs(String name, ArrayList<Person> mylist) {
		for (Person p : mylist) {
			if(p.name.equals(name)) {
				p.print();
			}
	 }
	}
	public static void countGender(ArrayList<Person> mylist) {
		int fem=0;
		int mal =0;
		for (Person p : mylist) {
			if(p.gender.equals("female")) {
				fem++;
			}else {
				mal++;
			}
	 }
		System.out.println("This dataset has "+ fem + " females and "+mal+" males.");
	}
	
	public static void hasWork(ArrayList<Person> mylist) {
		System.out.println("These people have work experience: ");
		for (Person p : mylist) {
			if(p.workExp) {
				System.out.print(mylist.indexOf(p)+".");
				System.out.println(p.name);
			}
	 }
		
	}
	

	public static void main(String[] args) {
		
		//create dataset
		Student yy = new Student("Yuchen Yang", "China", "female","Harbin Institute of Technology", false, "dancing and skiing","yy227", "ECE");
		Instructor af = new Instructor("Adel Fahmy", "Egypt", "male","NC State Univ." ,true, "tennis, biking, gardening, and cooking","Adjunct Assistant Professor");
		Instructor rt = new Instructor("Ric Telford","US", "male","Trinity University", true, "golf, sand Volleyball, swimming and biking","Adjunct Assoc. Professor");
		Instructor yy2 = new Instructor("Yuanyuan Yu", "China", "female","ECUST" ,true, "baseball and fencing","TA");
		Instructor yl = new Instructor("You Lyu", "China", "male","U.K." ,true, "traveling, music and history","TA");
		Instructor zl = new Instructor("Zhongyu Li", "China", "male","SouthEast university" ,true, "basketball & NBA","TA");
		Instructor lc = new Instructor("Lei Chen", "China", "female","KAIST" ,true, "climbing and animals","TA");
		Person lc2 = new Person("Chao Liu", "China", "male", "Fudan University", false, "jiujiuing Xiao Bao");
		
		
		ArrayList<Person> myList = new ArrayList<Person>();
		myList.add(yy);
		myList.add(af);
		myList.add(rt);
		myList.add(yy2);
		myList.add(yl);
		myList.add(zl);
		myList.add(lc);
		myList.add(lc2);
		
		//test functions
		whoIs("Yuchen Yang",myList);
		countGender(myList);
		hasWork(myList);

	}

}
