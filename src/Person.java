public class Person{
	String name;
	String Country;
	String gender;
	String edu;
	boolean workExp;
	String hobby;
	
	public Person(String name,String Country, String gender, String edu,boolean workExp,String hobby) {
		this.name = name;
		this.Country = Country;
		this.gender = gender;
		this.edu = edu;
		this.workExp = workExp;
		this.hobby = hobby;
	}
	
	public void print(){
		
		String demPron = new String("He");
		System.out.print(name + " is from "+ Country +". ");
		if(gender.equals("female")) {
			demPron = new String("She");
		}
		
		System.out.print(demPron + " graduated from " + edu + " and ");
		if(workExp) {
			System.out.println("has work experience before.");
		}else {
			System.out.println("does not have work experience before.");
		}
		System.out.println(demPron+" enjoys "+hobby+" the most during weekend.");
	}
}


