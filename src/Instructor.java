
public class Instructor extends BlueDevil {
	String title;
	public Instructor(String name, String Country, String gender,
			String edu,boolean workExp, String hobby,String title) {
		super(name, Country, gender,edu, workExp,hobby);
		this.title = title;
	}
	
	public void print() {
		super.print();
		String demPron = new String("he");
		if(gender.equals("female")) {
			demPron = new String("she");
		}
		if(title.charAt(0)=='a'||title.charAt(0)=='A'||title.charAt(0)=='e'||
				title.charAt(0)=='E'||title.charAt(0)=='O'||title.charAt(0)=='o') {
		System.out.println(demPron + " is an " + title +".");
		}else {
			System.out.println(demPron + " is a " + title +".");
		}
	}
}
