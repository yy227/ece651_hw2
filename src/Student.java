
public class Student extends BlueDevil{
	String netid;
	String major;
	public Student(String name, String Country, String gender,
			String edu,boolean workExp, String hobby,String netid,String major) {
		super(name, Country, gender,edu, workExp,hobby);
		this.netid = netid;
		this.major = major;
	}
	public void print() {
		super.print();
		String demPron = new String("his");
		if(gender.equals("female")) {
			demPron = new String("her");
		}
		if(netid.isEmpty()) {
			System.out.println(demPron + " major is "+ major + ".");
			
		}else {
		System.out.println(demPron + " major is "+ major + " and "+ demPron + " netid is "+ netid +".");
		}
	}

}
